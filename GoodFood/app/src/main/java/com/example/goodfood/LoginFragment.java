package com.example.goodfood;

import android.content.Intent;
import android.os.Bundle;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.example.goodfood.Models.User;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.concurrent.ExecutionException;

public class LoginFragment extends Fragment {

    EditText emailInput;
    EditText passwordInput;
    TextInputLayout emailInputLayout;
    TextInputLayout passwordInputLayout;
    Button logInButton;
    Button forgotButton;
    UserLocalStore userLocalStore;

    public LoginFragment() {
        // Required empty public constructor
    }

    /**
     * @brief Create the view
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_login_activity, container, false);
        userLocalStore = new UserLocalStore(this.getContext());
        emailInput = view.findViewById(R.id.editTextLoginEmail);
        passwordInput = view.findViewById(R.id.editTextLoginPassword);
        emailInputLayout = view.findViewById(R.id.loginInputLayoutEmail);
        passwordInputLayout = view.findViewById(R.id.loginInputLayoutPassword);
        logInButton = view.findViewById(R.id.buttonLogin);
        forgotButton = view.findViewById(R.id.buttonForgot);
        logInButton.setOnClickListener(this::Login);
        forgotButton.setOnClickListener(this::forgotPassword);
        return view;
    }

    
    /**
     * Function assign to the button on the view
     * @param v
     * Necessary to assign on a button
     */
    public void Login(View v) {
        boolean isValid = true;
        if(GlobalContext.isTextValid(emailInput.getText().toString()) == "empty"){
            emailInputLayout.setError("Email cannot be empty");
            isValid = false;
        }

        if(GlobalContext.isTextValid(passwordInput.getText().toString()) == "empty"){
            passwordInputLayout.setError("Email cannot be empty");
            isValid = false;
        }

        if(!isValid)
            return;

        String result = null;
        APIVela getRequest = new APIVela();
        String api = "logIn";
        String email = emailInput.getText().toString();
        String password = passwordInput.getText().toString();
        try {
            result = getRequest.execute(api, email, password).get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        if(result == null){
            Toast toast = Toast.makeText(getActivity().getApplicationContext(),"Invalid credentials" , Toast.LENGTH_LONG);
            toast.show();
        } else {
            String userId = "";
            String sessionToken = "";
            String newjs = "";
            JSONObject jsonObject;
            try {
                jsonObject = new JSONObject(result);
                sessionToken = jsonObject.getString("token");

                byte[] decodedBytes = Base64.decode(sessionToken, Base64.URL_SAFE);
                newjs = new String(decodedBytes, "UTF-8");
                jsonObject = new JSONObject(newjs.substring(15));
                userId = jsonObject.getString("userID");
            } catch (JSONException | UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            Toast toast = Toast.makeText(getActivity().getApplicationContext(),"Connection succeed" , Toast.LENGTH_LONG);
            toast.show();

            GlobalContext.setToken(sessionToken);

            User user = new User(userId,email,password,"","","","");
            userLocalStore.storeUserData(user);
            userLocalStore.setUserLoggedIn(true);

            Intent intent = new Intent(getContext(), ChooseRestaurant.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK );
            startActivity(intent);
            getActivity().finish();
        }
    }

    /**
     * @brief call forgetPassword API
     */
    public void forgotPassword(View v ){
        boolean isValid = true;
        if(GlobalContext.isTextValid(emailInput.getText().toString()) == "empty"){
            emailInputLayout.setError("Fill email field to receive an email");
            isValid = false;
        }

        if(!isValid)
            return;

        String result = null;
        APIVela getRequest = new APIVela();
        String api = "forgotPassword";
        String email = emailInput.getText().toString();
        try {
            result = getRequest.execute(api, email).get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
    }
}