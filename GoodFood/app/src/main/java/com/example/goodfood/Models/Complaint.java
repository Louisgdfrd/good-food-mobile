package com.example.goodfood.Models;

import java.util.List;

/**
 * @brief class to manipulate complaint
 */
public class Complaint {

    public Complaint(){

    }

    public String id;
    public String customer;
    public String order;
    public String type;
    public List<Ingredient> details;
    public String photo;
    public String description;
    public String status;
    public String action;
    public int value;
}
