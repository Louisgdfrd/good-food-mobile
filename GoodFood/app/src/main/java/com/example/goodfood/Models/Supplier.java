package com.example.goodfood.Models;

import java.util.List;
/**
 * @brief class to manipulate supplier
 */
public class Supplier {
    /**
     * @brief Constructor
     */
    public Supplier(String id,String name,boolean isStandard,String phone,String email,List<Ingredient> ingredients){
        this.id = id;
        this.name = name;
        this.isStandard = isStandard;
        this.phone = phone;
        this.email = email;
        this.ingredients = ingredients;

    }

    public final String id;
    public final String name;
    public final boolean isStandard;
    public final String phone;
    public final String email;
    public final List<Ingredient> ingredients;
}
