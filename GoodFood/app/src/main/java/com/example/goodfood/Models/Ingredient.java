package com.example.goodfood.Models;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
/**
 * @brief class to manipulate ingredient
 */
public class Ingredient {
    public Ingredient(String id,String name,List<String> allergen){
        this.id = id;
        this.name = name;
        this.allergen = allergen;
    }

    public Ingredient(){
        id = "";
        name = "Carrot";
        allergen.add("carrot");
    }

    public String id;
    public String name;
    public int quantity;
    public List<String> allergen = new ArrayList<>();

    public Ingredient(JSONObject json) {
        try {
            id = json.getString("id");
            name = json.getString("name");
            if(json.getJSONArray("allergen") != null){
                JSONArray allergenArray = json.getJSONArray("allergen");
                for (int i = 0;i < allergenArray.length();i++)
                    allergen.add(allergenArray.getString(i));
            } else {
                allergen.add("Pas d'allergènes");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
