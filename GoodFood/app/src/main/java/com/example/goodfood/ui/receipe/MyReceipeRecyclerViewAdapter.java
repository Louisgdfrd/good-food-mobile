package com.example.goodfood.ui.receipe;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.goodfood.GlobalContext;
import com.example.goodfood.Models.Recipe;
import com.example.goodfood.R;
import com.example.goodfood.databinding.FragmentReceipeChoiceBinding;

import java.util.List;

public class MyReceipeRecyclerViewAdapter extends RecyclerView.Adapter<MyReceipeRecyclerViewAdapter.ViewHolder> {

    private final List<Recipe> mValues;
    private final ReceipeChoiceFragment mFragment;

    public MyReceipeRecyclerViewAdapter(List<Recipe> items, ReceipeChoiceFragment fragment) {
        mValues = items;
        mFragment = fragment;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(FragmentReceipeChoiceBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, @SuppressLint("RecyclerView") int position) {
        holder.mItem = mValues.get(position);
        holder.mReceipeName.setText(mValues.get(position).name);
        holder.mReceipeName.setTypeface(null, Typeface.BOLD);
        //holder.mReceipeAllergens.setText("Allergens : " + mValues.get(position).getAllergens());
        //holder.mReceipeIngredient.setText(mValues.get(position).getIngredientsText());
        holder.mReceipePrice.setText(Float.toString(mValues.get(position).getRealPrice()));
        holder.mImageView.setImageResource(R.drawable.no_photography);
        if(mValues.get(position).photo != "" ) {
            byte[] imageAsBytes = Base64.decode(mValues.get(position).photo.getBytes(),Base64.DEFAULT);
            Bitmap bp = BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length);
            holder.mImageView.setImageBitmap(bp);
        }
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final TextView mReceipeName;
        public final CardView mCard;
        //public final TextView mReceipeIngredient;
        //public final TextView mReceipeAllergens;
        public final TextView mReceipePrice;
        public final ImageView mImageView;
        public Recipe mItem;

        public ViewHolder(FragmentReceipeChoiceBinding binding) {
            super(binding.getRoot());
            mReceipeName = binding.choiceReceipeName;
            //mReceipeIngredient = binding.choiceReceipeIngredients;
            mCard = binding.cardViewReceipe;
            mReceipePrice = binding.choiceReceipePrice;
            //mReceipeAllergens = binding.choiceReceipeAllergen;
            mImageView = binding.recipeIcon;
            mCard.setOnLongClickListener(this::changeVisibility);
            mCard.setOnClickListener(this::clicked);
        }

        public void clicked(View view){
            GlobalContext.addToCart(mItem);
        }
        public boolean changeVisibility(View view){
            /**
            if(mReceipeAllergens.getVisibility() == View.VISIBLE)
                mReceipeAllergens.setVisibility(View.GONE);
            else
                mReceipeAllergens.setVisibility(View.VISIBLE);
             */
            return true;
        }
    }
}