package com.example.goodfood;

import android.os.AsyncTask;

import com.example.goodfood.Models.NameValuePair;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class APIVela extends AsyncTask<String, Void, String> {

    //"http://localhost:8082

    /**
     * Function to call the API
     * @param params
     * List of string [0] is for the function you want to call the next ones arguments for the function.
     * @return String
     * Result of the function you have called
     */
    @Override
    protected String doInBackground(String... params) {
        String result = null;
        String action = params[0];
        switch (action){
            case "logIn":
                result = logIn(params[1],params[2]);
                break;
            case "createAccount":
                result = createAccount(params[1],params[2],params[3]);
                break;
            case "getUser":
                result = getUser();
                break;
            case "editUser":
                result = editUser(params[1],params[2],params[3],params[4],params[5]);
                break;
            case "editPassword":
                result = editPassword(params[1],params[2]);
                break;
            case "getRestaurant":
                result = getRestaurant(params[1]);
                break;
            case "getAllRestaurant":
                result = getAllRestaurant();
                break;
            case "getIngredient":
                result = getIngredient(params[1]);
                break;
            case "getAllIngredient":
                result = getAllIngredient();
                break;
            case "getRestaurantReceipe":
                result = getRestaurantReceipe();
                break;
            case "forgotPassword":
                result = forgotPassword(params[1]);
                break;
            case "addAdress":
                result = addAdress(params[1],params[2],params[3],params[4]);
                break;
            case "removeAdress":
                result = removeAdress(params[1]);
                break;
            case "getAdress":
                result = getAdress();
                break;
            case "sendFeedback":
                result = sendFeedback(params[1],params[2]);
                break;
            case "order":
                result = order(params[1]);
                break;
        }
        return result;
    }

    /**
     * @brief used in case you need parameters in url
     * @return String to add in url
     */
    private static String getQuery(List<NameValuePair> params){
        StringBuilder result = new StringBuilder();
        boolean first = true;

        for (NameValuePair pair : params)
        {
            if (first)
                first = false;
            else
                result.append("&");

            try {
                result.append(URLEncoder.encode(pair.getName(), "UTF-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            result.append("=");
            try {
                result.append(URLEncoder.encode(pair.getValue(), "UTF-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }

        return result.toString();
    }

    /**
     * Returns if the user mail and password match in database for log in
     * @param params
     * User mail and password
     * @return String
     * Return if the user exist
     */
    public static String logIn(String... params){
        String urlAddress = "http://10.0.2.2:8082/login?";
        String result;
        String username = params[0];
        String password = params[1];
        try {
            List<NameValuePair> list = new ArrayList<>();
            list.add(new NameValuePair("username",username));
            list.add(new NameValuePair("password",password));
            urlAddress += getQuery(list);
            URL url = new URL(urlAddress);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept","application/json");
            conn.setDoOutput(true);
            conn.setDoInput(true);

            try(BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), StandardCharsets.UTF_8))) {
                StringBuilder response = new StringBuilder();
                String responseLine;
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }
                result = response.toString();
            } catch (Exception e) {
                e.printStackTrace();
                result = null;
            } finally {
                conn.disconnect();
            }
        } catch (Exception e) {
            e.printStackTrace();
            result = null;
        }
        System.out.println("DEBUG " + result);
        return result;
    }

    /**
     * Function to create a new account
     * @param params
     * User mail and password
     * @return String
     * Return if creation of the account is successful
     */
    public static String createAccount(String... params){
        String urlAddress = "http://10.0.2.2:8082/register";
        String result;
        String username = params[0];
        String password = params[1];
        Boolean consent = true;
        DateFormat simpleDateFormat= new SimpleDateFormat("dd/MM/yyyy");
        try {
            JSONObject jsonParam = new JSONObject();
            JSONObject jsonRgpd = new JSONObject();
            jsonParam.put("email", username);
            jsonParam.put("password", password);
            jsonRgpd.put("hasConsentForDB", consent);
            jsonRgpd.put("dbConsentDate", simpleDateFormat.format(new Date()));
            jsonParam.put("rgpd", jsonRgpd);

            URL url = new URL(urlAddress);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept","application/json");
            conn.setDoOutput(true);
            conn.setDoInput(true);

            OutputStream os = conn.getOutputStream();
            OutputStreamWriter osw = new OutputStreamWriter(os, "UTF-8");
            osw.write(jsonParam.toString());
            osw.flush();
            osw.close();
            os.close();
            conn.connect();

            try(BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), StandardCharsets.UTF_8))) {
                StringBuilder response = new StringBuilder();
                String responseLine;
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }
                result = response.toString();
            } catch (Exception e) {
                e.printStackTrace();
                result = null;
            } finally {
                conn.disconnect();
            }
        } catch (Exception e) {
            e.printStackTrace();
            result = null;
        }
        return result;
    }

    /**
     * @brief Function get a user profile informations
     * @return String
     */
    public static String getUser(){
        String urlAddress = "http://10.0.2.2:8082/customer/" + GlobalContext.getUser().id + "/information";
        String result = "";
        try {
            URL url = new URL(urlAddress);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept","application/json");
            conn.setRequestProperty("Authorization","Bearer " + GlobalContext.getToken());
            conn.setDoInput(true);

            try(BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), StandardCharsets.UTF_8))) {
                StringBuilder response = new StringBuilder();
                String responseLine;
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }
                result = response.toString();
            } catch (Exception e) {
                e.printStackTrace();
                result = null;
            } finally {
                conn.disconnect();
            }
        } catch (Exception e) {
            e.printStackTrace();
            result = null;
        }
        return result;
    }
    
    /**
     * Function to edit a user profile
     * @param params
     * New user data
     */
    public static String editUser(String... params){
        String urlAddress = "http://10.0.2.2:8082/customer/" + GlobalContext.getUser().id + "/information";
        String result;
        String firstName = params[0];
        String lastName = params[1];
        String phone = params[2];
        String birthdate = params[3];
        try {
            JSONObject jsonParam = new JSONObject();
            jsonParam.put("firstName", firstName);
            jsonParam.put("lastName", lastName);
            jsonParam.put("phone", phone);
            jsonParam.put("birthDate", birthdate);

            URL url = new URL(urlAddress);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod(params[4]);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept","application/json");
            conn.setRequestProperty("Authorization","Bearer " + GlobalContext.getToken());
            conn.setDoOutput(true);
            conn.setDoInput(true);

            OutputStream os = conn.getOutputStream();
            OutputStreamWriter osw = new OutputStreamWriter(os, "UTF-8");
            osw.write(jsonParam.toString());
            osw.flush();
            osw.close();
            os.close();
            conn.connect();

            try(BufferedReader br = new BufferedReader(
                    new InputStreamReader(conn.getInputStream(), StandardCharsets.UTF_8))) {
                StringBuilder response = new StringBuilder();
                String responseLine;
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }
                JSONObject myResp = new JSONObject(response.toString());
                String message = myResp.getString("message");
                result = message;
            } catch (Exception e) {
                result = null;
            } finally {
                conn.disconnect();
            }
        } catch (Exception e) {
            result = null;
        }
        return result;
    }

    /**
     * Function to edit User password
     * @param params
     * New password
     */
    public static String editPassword(String... params){
        String urlAddress = "http://10.0.2.2:8082/customer/changePassword/" + GlobalContext.getUser().id;
        String result;
        String oldPassword = params[0];
        String newPassword = params[1];
        try {
            JSONObject jsonParam = new JSONObject();
            jsonParam.put("oldPassword", oldPassword);
            jsonParam.put("newPassword", newPassword);

            URL url = new URL(urlAddress);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept","application/json");
            conn.setRequestProperty("Authorization","Bearer " + GlobalContext.getToken());
            conn.setDoOutput(true);
            conn.setDoInput(true);

            OutputStream os = conn.getOutputStream();
            OutputStreamWriter osw = new OutputStreamWriter(os, "UTF-8");
            osw.write(jsonParam.toString());
            osw.flush();
            osw.close();
            os.close();
            conn.connect();

            try(BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), StandardCharsets.UTF_8))) {
                StringBuilder response = new StringBuilder();
                String responseLine;
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }
                result = response.toString();
            } catch (Exception e) {
                e.printStackTrace();
                result = null;
            } finally {
                conn.disconnect();
            }
        } catch (Exception e) {
            e.printStackTrace();
            result = null;
        }
        return result;
    }

    /**
     * Function to get a specific restaurant
     * @param params
     * Uid of the restaurant
     * @return String
     * return the restaurant if it exist
     */
    public static String getRestaurant(String... params){
        String uid = params[0];
        String urlAddress = "http://10.0.2.2:8082/restaurant/" + uid;
        String result;
        try {
            URL url = new URL(urlAddress);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept","application/json");
            conn.setRequestProperty("Authorization","Bearer " + GlobalContext.getToken());
            conn.setDoInput(true);

            try(BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), StandardCharsets.UTF_8))) {
                StringBuilder response = new StringBuilder();
                String responseLine;
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }
                result = response.toString();
            } catch (Exception e) {
                e.printStackTrace();
                result = null;
            } finally {
                conn.disconnect();
            }
        } catch (Exception e) {
            e.printStackTrace();
            result = null;
        }
        return result;
    }

    /**
     * Get all the restaurant in database
     * @return String
     * return All the restaurants
     */
    public static  String getAllRestaurant(){
        String urlAddress = "http://10.0.2.2:8082/restaurant";
        String result;
        try {
            URL url = new URL(urlAddress);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept","application/json");
            conn.setRequestProperty("Authorization","Bearer " + GlobalContext.getToken());
            conn.setDoInput(true);

            try(BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), StandardCharsets.UTF_8))) {
                StringBuilder response = new StringBuilder();
                String responseLine;
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }
                result = response.toString();
            } catch (Exception e) {
                e.printStackTrace();
                result = null;
            } finally {
                conn.disconnect();
            }
        } catch (Exception e) {
            e.printStackTrace();
            result = null;
        }
        return result;
    }

    /**
     * @brief Get recipe of the restaurant choose
     * @return String
     */
    public static String getRestaurantReceipe(){
        String urlAddress = "http://10.0.2.2:8082/restaurant/" + GlobalContext.getRestaurantChoice().id + "/recipe";
        String result;
        try {
            URL url = new URL(urlAddress);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept","application/json");
            conn.setRequestProperty("Authorization","Bearer " + GlobalContext.getToken());
            conn.setDoInput(true);

            try(BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), StandardCharsets.UTF_8))) {
                StringBuilder response = new StringBuilder();
                String responseLine;
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }
                result = response.toString();
            } catch (Exception e) {
                e.printStackTrace();
                result = null;
            } finally {
                conn.disconnect();
            }
        } catch (Exception e) {
            e.printStackTrace();
            result = null;
        }
        return result;
    }
    
    /** 
     * Get a specific ingredient
     * @param params
     * Ingredient Uid
     * @return String
     * Return the ingredient if it exist
     */
    public static String getIngredient(String... params){
        String uid = params[0];
        String urlAddress = "http://10.0.2.2:8082/ingredient/" + uid;
        String result;

        try {
            URL url = new URL(urlAddress);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept","application/json");
            conn.setRequestProperty("Authorization","Bearer " + GlobalContext.getToken());
            conn.setDoInput(true);

            try(BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), StandardCharsets.UTF_8))) {
                StringBuilder response = new StringBuilder();
                String responseLine;
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }
                result = response.toString();
            } catch (Exception e) {
                e.printStackTrace();
                result = null;
            } finally {
                conn.disconnect();
            }
        } catch (Exception e) {
            e.printStackTrace();
            result = null;
        }
        return result;
    }

    
    /** 
     * return all the ingredient in database
     * @return String
     * All the ingredient in database
     */
    public static  String getAllIngredient(){
        String urlAddress = "http://10.0.2.2:8082/ingredient";
        String result;
        try {
            URL url = new URL(urlAddress);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept","application/json");
            conn.setRequestProperty("Authorization","Bearer " + GlobalContext.getToken());
            conn.setDoInput(true);

            try(BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), StandardCharsets.UTF_8))) {
                StringBuilder response = new StringBuilder();
                String responseLine;
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }
                result = response.toString();
            } catch (Exception e) {
                e.printStackTrace();
                result = null;
            } finally {
                conn.disconnect();
            }
        } catch (Exception e) {
            e.printStackTrace();
            result = null;
        }
        System.out.println("DEBUG " + result);
        return result;
    }

    /**
     * Function to call forgot password API
     * @param params
     * User mail
     */
    public static String forgotPassword(String... params){
        String urlAddress = "http://10.0.2.2:8082/customer/forgotPassword";
        String result;
        String email = params[0];
        try {
            JSONObject jsonParam = new JSONObject();
            jsonParam.put("email", email);

            URL url = new URL(urlAddress);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept","application/json");
            conn.setDoOutput(true);
            conn.setDoInput(true);

            OutputStream os = conn.getOutputStream();
            OutputStreamWriter osw = new OutputStreamWriter(os, "UTF-8");
            osw.write(jsonParam.toString());
            osw.flush();
            osw.close();
            os.close();
            conn.connect();

            try(BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), StandardCharsets.UTF_8))) {
                StringBuilder response = new StringBuilder();
                String responseLine;
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }
                result = response.toString();
            } catch (Exception e) {
                e.printStackTrace();
                result = null;
            } finally {
                conn.disconnect();
            }
        } catch (Exception e) {
            e.printStackTrace();
            result = null;
        }
        System.out.println("DEBUG " + result);
        return result;
    }

    /**
     * Function to add an adress to actual user
     * @param params
     * New adress
     */
    public static String addAdress(String... params){
        String urlAddress = "http://10.0.2.2:8082/customer/" + GlobalContext.getUser().id +"/address";
        String result;
        String number = params[0];
        String street = params[1];
        String postal = params[2];
        String city = params[3];
        try {
            JSONObject jsonParam = new JSONObject();
            jsonParam.put("number", number);
            jsonParam.put("street", street);
            jsonParam.put("postal", postal);
            jsonParam.put("city", city);

            URL url = new URL(urlAddress);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept","application/json");
            conn.setRequestProperty("Authorization","Bearer " + GlobalContext.getToken());
            conn.setDoOutput(true);
            conn.setDoInput(true);

            OutputStream os = conn.getOutputStream();
            OutputStreamWriter osw = new OutputStreamWriter(os, "UTF-8");
            osw.write(jsonParam.toString());
            osw.flush();
            osw.close();
            os.close();
            conn.connect();

            try(BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), StandardCharsets.UTF_8))) {
                StringBuilder response = new StringBuilder();
                String responseLine;
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }
                result = response.toString();
            } catch (Exception e) {
                e.printStackTrace();
                result = null;
            } finally {
                conn.disconnect();
            }
        } catch (Exception e) {
            result = null;
        }
        return result;
    }

    /**
     * Function to get adresses of the actual user
     * @return String
     * Return if an error occurred
     */
    public static String getAdress(){
        String urlAddress = "http://10.0.2.2:8082/customer/" + GlobalContext.getUser().id +"/address";
        String result;
        try {
            URL url = new URL(urlAddress);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept","application/json");
            conn.setRequestProperty("Authorization","Bearer " + GlobalContext.getToken());
            conn.setDoInput(true);

            try(BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), StandardCharsets.UTF_8))) {
                StringBuilder response = new StringBuilder();
                String responseLine;
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }
                result = response.toString();
            } catch (Exception e) {
                e.printStackTrace();
                result = null;
            } finally {
                conn.disconnect();
            }
        } catch (Exception e) {
            result = null;
        }
        return result;
    }

    /**
     * Remove an adress for a user
     * @param params address id to remove
     */
    public static String removeAdress(String... params){
        String adressId = params[0];
        String urlAddress = "http://10.0.2.2:8082/customer/" + GlobalContext.getUser().id +"/address/" + adressId;
        String result;
        try {
            URL url = new URL(urlAddress);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("DELETE");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept","application/json");
            conn.setRequestProperty("Authorization","Bearer " + GlobalContext.getToken());
            conn.setDoOutput(true);
            conn.setDoInput(true);

            try(BufferedReader br = new BufferedReader(
                    new InputStreamReader(conn.getInputStream(), StandardCharsets.UTF_8))) {
                StringBuilder response = new StringBuilder();
                String responseLine;
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }
                JSONObject myResp = new JSONObject(response.toString());
                String message = myResp.getString("message");
                result = message;
            } catch (Exception e) {
                result = null;
            } finally {
                conn.disconnect();
            }
        } catch (Exception e) {
            result = null;
        }
        return result;
    }

    /**
     * Send a feedback from user
     * @param params
     * Comment and rating
     */
    public static String sendFeedback(String... params){
        String urlAddress = "http://10.0.2.2:8082/restaurant/" + GlobalContext.getRestaurantChoice().id + "/feedback";
        String result;
        String comment = params[0];
        String eval = params[1];

        try {
            JSONObject jsonParam = new JSONObject();
            jsonParam.put("comment", comment);
            jsonParam.put("eval", Integer.parseInt(eval));
            System.out.println("DEBUG " + jsonParam.toString());

            URL url = new URL(urlAddress);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Accept","application/json");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Authorization","Bearer " + GlobalContext.getToken());
            conn.setDoOutput(true);
            conn.setDoInput(true);

            OutputStream os = conn.getOutputStream();
            OutputStreamWriter osw = new OutputStreamWriter(os, "UTF-8");
            osw.write(jsonParam.toString());
            osw.flush();
            osw.close();
            os.close();
            conn.connect();

            try(BufferedReader br = new BufferedReader(
                    new InputStreamReader(conn.getInputStream(), StandardCharsets.UTF_8))) {
                StringBuilder response = new StringBuilder();
                String responseLine;
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }
                JSONObject myResp = new JSONObject(response.toString());
                String message = myResp.getString("message");
                result = message;
            } catch (Exception e) {
                result = null;
            } finally {
                conn.disconnect();
            }
        } catch (Exception e) {
            result = null;
        }
        return result;
    }

    /**
     * Create a new order
     * @param params
     * @return
     * Return id of the order or if an error occurred
     */
    public static String order(String... params){
        String urlAddress = "http://10.0.2.2:8082/customer/" + GlobalContext.getUser().id + "/order";
        String result;
        String order = params[0];
        try {
            URL url = new URL(urlAddress);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Accept","application/json");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Authorization","Bearer " + GlobalContext.getToken());
            conn.setDoOutput(true);
            conn.setDoInput(true);

            OutputStream os = conn.getOutputStream();
            OutputStreamWriter osw = new OutputStreamWriter(os, "UTF-8");
            osw.write(order);
            osw.flush();
            osw.close();
            os.close();
            conn.connect();

            try(BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), StandardCharsets.UTF_8))) {
                StringBuilder response = new StringBuilder();
                String responseLine;
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }
                result = response.toString();
            } catch (Exception e) {
                e.printStackTrace();
                result = null;
            } finally {
                conn.disconnect();
            }
        } catch (Exception e) {
            result = null;
        }
        return result;
    }
}
