package com.example.goodfood;

import android.content.Intent;
import android.os.Bundle;
import android.util.Base64;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.example.goodfood.Models.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

public class MainActivity extends AppCompatActivity {

    UserLocalStore userLocalStore;

    /**
     * Create view and verify logged in user to launch app or not
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        userLocalStore = new UserLocalStore(this);
        GlobalContext.clearCart();
        if (userLocalStore.getUserLoggedIn()){
            User user = userLocalStore.getLoggedInUser();
            String result = null;
            APIVela getRequest = new APIVela();
            String api = "logIn";
            System.out.println(user.email + user.password);
            try {
                result = getRequest.execute(api, user.email, user.password).get();
            } catch (ExecutionException | InterruptedException e) {
                e.printStackTrace();
            }
            if(result != null) {
                JSONObject jsonObject = null;
                String token = new String();
                String newjs = new String();
                try {
                    jsonObject = new JSONObject(result);
                    token = jsonObject.getString("token");

                    byte[] decodedBytes = Base64.decode(token, Base64.URL_SAFE);
                    newjs = new String(decodedBytes, "UTF-8");
                    jsonObject = new JSONObject(newjs.substring(15));
                    user.id = jsonObject.getString("userId");
                } catch (JSONException | UnsupportedEncodingException e) {}
                GlobalContext.setToken(token);
                GlobalContext.setUser(user);

                Intent intent = new Intent(this, ChooseRestaurant.class);
                startActivity(intent);
                super.finish();
            }
        }

        setContentView(R.layout.activity_main);

        ViewPager viewPager = findViewById(R.id.viewPager);

        AuthenticationPagerAdapter pagerAdapter = new AuthenticationPagerAdapter(getSupportFragmentManager());
        pagerAdapter.addFragmet(new LoginFragment());
        pagerAdapter.addFragmet(new RegisterFragment());
        viewPager.setAdapter(pagerAdapter);
    }

    /**
     * Class to manage Page View
     */
    static class AuthenticationPagerAdapter extends FragmentPagerAdapter {
        private final ArrayList<Fragment> fragmentList = new ArrayList<>();

        public AuthenticationPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @NonNull
        @Override
        public Fragment getItem(int i) {
            return fragmentList.get(i);
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }

        void addFragmet(Fragment fragment) {
            fragmentList.add(fragment);
        }
    }
}