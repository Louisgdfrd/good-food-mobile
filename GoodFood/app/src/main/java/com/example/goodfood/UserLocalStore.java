package com.example.goodfood;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.goodfood.Models.User;

public class UserLocalStore {

    public static final String SP_NAME = "userDetails";
    final SharedPreferences userLocalDatabase;

    /**
     * @brief
     * Constructor
     */
    public UserLocalStore(Context context){
        userLocalDatabase = context.getSharedPreferences(SP_NAME,0);
    }

    /**
     * @brief
     * Add user in the local store
     */
    public void storeUserData(User user){
        SharedPreferences.Editor spEditor = userLocalDatabase.edit();
        spEditor.putString("id",user.id);
        spEditor.putString("email",user.email);
        spEditor.putString("password",user.password);
        spEditor.putString("firstName",user.firstName);
        spEditor.putString("lastName",user.lastName);
        spEditor.putString("phone",user.phone);
        spEditor.putString("birthdate",user.birthdate);
        spEditor.commit();
    }

    /**
     * @brief
     * Return Logged in user
     */
    public User getLoggedInUser(){
        String id = userLocalDatabase.getString("id","");
        String email = userLocalDatabase.getString("email","");
        String password = userLocalDatabase.getString("password","");
        String firstName = userLocalDatabase.getString("firstName","");
        String lastName = userLocalDatabase.getString("lastName","");
        String phone = userLocalDatabase.getString("phone","");
        String birthDate = userLocalDatabase.getString("birthdate","");
        return new User(id,email,password,firstName,lastName,phone,birthDate);
    }

    /**
     * @brief
     * Change logged in status
     */
    public void setUserLoggedIn(boolean logged){
        SharedPreferences.Editor spEditor = userLocalDatabase.edit();
        spEditor.putBoolean("loggedIn",logged);
        spEditor.commit();
    }

    /**
     * @brief
     * Get the logged in user
     */
    public boolean getUserLoggedIn(){
        if(userLocalDatabase.getBoolean("loggedIn", false)){
            return true;
        }else{
            return false;
        }
    }

    /**
     * @brief
     * Clear User data
     */
    public void clearUserData(){
        SharedPreferences.Editor spEditor = userLocalDatabase.edit();
        spEditor.clear();
        spEditor.commit();
    }
}
