package com.example.goodfood.ui.Profile;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.example.goodfood.APIVela;
import com.example.goodfood.AdressManagment;
import com.example.goodfood.ChangePassword;
import com.example.goodfood.MainActivity;
import com.example.goodfood.ModifyProfile;
import com.example.goodfood.R;
import com.example.goodfood.UserLocalStore;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.ExecutionException;

public class ProfileFragment extends Fragment {

    private View binding;
    private Button mButtonInformation;
    private Button mButtonPassword;
    private Button mButtonLogOut;
    private Button mButtonAdresses;
    private  TextView mFirstName;
    private  TextView mLastName;
    private  TextView mPhone;
    private  TextView mBirthdate;
    private UserLocalStore mUserLocalStore;

    /**
     * @brief set view elements
     */
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        HomeViewModel homeViewModel = new ViewModelProvider(this).get(HomeViewModel.class);

        binding = inflater.inflate(R.layout.fragment_profile, container, false);
        mButtonInformation = binding.findViewById(R.id.changeInformationButton);
        mButtonAdresses = binding.findViewById(R.id.manageAdressButton);
        mButtonLogOut = binding.findViewById(R.id.logOutButton);
        mFirstName = binding.findViewById(R.id.profileFirstName);
        mLastName = binding.findViewById(R.id.profileLastName);
        mPhone = binding.findViewById(R.id.profilePhone);
        mBirthdate = binding.findViewById(R.id.profileBirthDate);
        mButtonPassword = binding.findViewById(R.id.changePasswordButton);
        mUserLocalStore = new UserLocalStore(this.getContext());
        mButtonInformation.setOnClickListener(this::changeInformation);
        mButtonLogOut.setOnClickListener(this::logOut);
        mButtonAdresses.setOnClickListener(this::manageAdresses);
        mButtonPassword.setOnClickListener(this::changePassword);

        return binding;
    }

    /**
     * @brief change the view to change informations activity
     */
    public void changeInformation(View v){
        Intent intent = new Intent(getContext(), ModifyProfile.class);
        startActivity(intent);
    }

    /**
     * @brief change the view to change adresses
     */
    public void manageAdresses(View v){
        Intent intent = new Intent(getContext(), AdressManagment.class);
        startActivity(intent);
    }

    /**
     * @brief change the view to change password
     */
    public void changePassword(View v){
        Intent intent = new Intent(getContext(), ChangePassword.class);
        startActivity(intent);
    }

    /**
     * @brief Logout and return to login/register
     */
    public void logOut(View v){
        mUserLocalStore.clearUserData();
        mUserLocalStore.setUserLoggedIn(false);
        Intent intent = new Intent(this.getContext(), MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK );
        startActivity(intent);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    @Override
    public void onResume() {
        setTextFragment();
        super.onResume();
    }

    /**
     * @brief set content of view elements
     */
    private void setTextFragment(){
        String result = null;
        APIVela getRequest = new APIVela();
        String api = "getUser";
        try {
            result = getRequest.execute(api).get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        if(result == null){
            Toast toast = Toast.makeText(getActivity().getApplicationContext(),"An error as occurred" , Toast.LENGTH_LONG);
            toast.show();
        } else {
            String firstName = "Not set";
            String lastName = "Not set";
            String phone = "Not set";
            String birthdate = "Not set";

            if(!result.equals(""))
            {
                JSONObject jsonObject;
                try {
                    jsonObject = new JSONObject(result);
                    firstName = jsonObject.getString("firstName");
                    lastName = jsonObject.getString("lastName");
                    phone = jsonObject.getString("phone");
                    birthdate = jsonObject.getString("birthDate");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            mFirstName.setText(firstName);
            mLastName.setText(lastName);
            mPhone.setText(phone);
            mBirthdate.setText(birthdate);
        }
    }
}