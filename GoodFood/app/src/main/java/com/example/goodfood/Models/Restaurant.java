package com.example.goodfood.Models;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
/**
 * @brief class to manipulate restaurant
 */
public class Restaurant {
    /**
     * @brief Constructor
     */
    public Restaurant(){
        id = "1";
        name = "goodFood";
        receipes = new ArrayList<>();
        receipes.add(new Recipe());
        receipes.add(new Recipe());
        adress = new Adress();
        discount = new Discount();
        phone = "0123456789";
    }

    public Restaurant(JSONObject json){
        try {
            id = json.getString("id");
            name = json.getString("name");
            owner = new RestaurantOwner(json.getJSONObject("owner"));
            adress = new Adress(json.getJSONObject("address"));
            phone = json.getString("phone");
            receipes = new ArrayList<>();
            JSONArray recipesArray = json.getJSONArray("recipes");
            for (int i = 0; i < recipesArray.length(); i++)
                receipes.add(new Recipe(recipesArray.getJSONObject(i)));

            if (!json.isNull("discount")){
                discount = new Discount(json.getJSONObject("discount"));
                discount.value = json.getJSONObject("discount").getInt("value");
            }
            feedbacks = new ArrayList<>();
            JSONArray feedbackArray = json.getJSONArray("recipes");
            for(int i = 0;i < feedbackArray.length();i++)
                feedbacks.add(new Feedback(feedbackArray.getJSONObject(i)));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String id;
    public String name;
    public RestaurantOwner owner;
    public Adress adress;
    public Discount discount = new Discount();
    public String phone;
    public Date openDateTime;
    public Date closeDateTime;
    public List<Recipe> receipes;
    public List<Supplier> suppliers;
    public List<Feedback> feedbacks;
}
