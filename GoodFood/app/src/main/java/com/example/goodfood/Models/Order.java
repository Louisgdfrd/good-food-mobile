package com.example.goodfood.Models;

import com.example.goodfood.GlobalContext;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @brief class to manipulate Order
 */
public class Order {

    public Order(){
        customer = GlobalContext.getUser().id;
        adress = GlobalContext.orderAdress.id;
        restaurant = GlobalContext.getRestaurantChoice().id;
        details = GlobalContext.getCart();
        if(adress.equals(GlobalContext.getRestaurantChoice().adress.id))
            type = "take-away";
        else
            type = "delivery";
        readyTime = new Date();
        arrivalTime = new Date(readyTime.getTime() + (1000 * 60 * 60 * 24));
    }

    public String id;
    public String customer;
    public String adress;
    public String restaurant;
    public double discountValue = 0;
    public Date orderDate;
    public Double total;
    public Boolean isPayed;
    public String paymentRef;
    public String status;
    public List<Recipe> details;
    public String type;
    public Date readyTime;
    public Date arrivalTime;

    public String sendOrderToJson(){
        DateFormat simpleDateFormat= new SimpleDateFormat("dd/MM/yyyy");

        String order = "{ \"customer\": \""+ GlobalContext.getUser().id + "\", \"address\": { \"id\": \""+ GlobalContext.orderAdress.id + "\" },\"restaurant\": \""+ GlobalContext.getRestaurantChoice().id +  "\", \"details\": [ { \"recipe\": \"" + GlobalContext.getCart().get(0).id + "\", \"quantity\": 1, \"price\": 1 }], \"delivery\": { \"type\": \"take-away\", \"readyTime\": \""+ simpleDateFormat.format(readyTime) + "\", \"arrivalTime\": \"2022-05-22T13:29:12.960Z\" } }";

        JSONObject json = new JSONObject();
        JSONObject jsonAdress = new JSONObject();
        JSONArray recipes = new JSONArray();
        JSONObject delivery = new JSONObject();
        try {
            json.put("customer", customer);

            jsonAdress.put("id",adress);
            json.put("address", jsonAdress);

            json.put("restaurant", restaurant);

            for(int i = 0; i < details.size();i++){
                JSONObject newRecipe = new JSONObject();
                newRecipe.put("recipe",details.get(i).id);
                newRecipe.put("quantity",1);
                newRecipe.put("price",details.get(i).getRealPrice());
                recipes.put(newRecipe);
            }
            json.put("details", recipes);

            delivery.put("type",type);
            delivery.put("readyTime",simpleDateFormat.format(readyTime));
            delivery.put("arrivalTime","2022-05-22T13:29:12.960Z");
            json.put("delivery", delivery);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return order.toString();
    }
}
