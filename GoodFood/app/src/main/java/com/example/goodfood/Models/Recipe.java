package com.example.goodfood.Models;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
/**
 * @brief class to manipulate Recipe
 */
public class Recipe {

    public Recipe(){
        id = "1";
        name = "boulgour";
        discount = new Discount();
        ingredients = new ArrayList<>();
        ingredients.add(new Ingredient());
    }

    /**
     * @brief Constructor to use with API response
     */
    public Recipe(JSONObject json){
        try {
            id = json.getString("id");
            name = json.getString("name");
            photo = json.getString("photo");
            if(!json.isNull("discount")) {
                discount = new Discount(json.getJSONObject("discount"));
                discount.value = json.getJSONObject("discount").getInt("value");
            } else
                discount = new Discount();
            standardPrice = json.getInt("price");
            standardPrice = standardPrice/100;
            if(!json.isNull("ingredients")){
                JSONArray ingredientsArray = json.getJSONArray("ingredients");
                ingredients = new ArrayList<>();
                for (int i = 0;i < ingredientsArray.length();i++)
                    ingredients.add(new Ingredient(ingredientsArray.getJSONObject(i)));
            } else {
                ingredients.add(new Ingredient());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * @brief get a price with discount
     */
    public float getRealPrice(){
        if (discount.value != 0)
            return (1-(discount.value/100))*standardPrice;
        else
            return standardPrice;
    }

    /**
     * @brief get all the ingredients of the recipe
     */
    public String getAllergens(){
        String result = "";
        for (int i = 0;i < ingredients.size();i++){
            for (int j = 0;j < ingredients.get(i).allergen.size();j++){
                if(!result.contains(ingredients.get(i).allergen.get(j)) &&
                        !ingredients.get(i).allergen.get(j).contains("pas d'allergènes") &&
                        !ingredients.get(i).allergen.get(j).contains("aucun") ) {
                    result += ingredients.get(i).allergen.get(j);
                    result += ", ";
                }
            }
        }
        if(result.length() == 0)
            result += "Pas d'allergènes";
        return result+ ".";
    }

    /**
     * @brief get ingredient text for recipe informations
     */
    public String getIngredientsText(){
        String result = "";
        for(int i =0;i < ingredients.size();i++) {
            result += ingredients.get(i).name;
            if(i != ingredients.size()-1)
                result += ", ";
        }
        return result + ".";
    }

    public String id;
    public String owner;
    public String name;
    public String photo;
    public float standardPrice = 0;
    public Discount discount;
    public boolean isStandard;
    public String creator;
    public List<Ingredient> ingredients = new ArrayList<>();
}
