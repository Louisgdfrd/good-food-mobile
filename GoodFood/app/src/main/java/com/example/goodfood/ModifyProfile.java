package com.example.goodfood;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.ExecutionException;

public class ModifyProfile extends AppCompatActivity {

    private Button mButton;
    private EditText mEditFirstName;
    private EditText mEditLastName;
    private EditText mEditPhone;
    private EditText mEditBirthDate;

    private TextInputLayout mEditFirstNameLayout;
    private TextInputLayout mEditLastNameLayout;
    private TextInputLayout mEditPhoneLayout;
    private TextInputLayout mEditBirthDateLayout;
    public String infoSet = "POST";
    private UserLocalStore mUserLocalStore;

    /**
     * @brief set view elements
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mUserLocalStore = new UserLocalStore(this);
        setContentView(R.layout.activity_modify_profile);
        mButton = findViewById(R.id.saveInformationButton);
        mEditFirstName = findViewById(R.id.profileEditFirstName);
        mEditLastName = findViewById(R.id.profileEditLastName);
        mEditPhone = findViewById(R.id.profileEditPhone);
        mEditBirthDate = findViewById(R.id.profileEditBirthDate);

        String result = null;
        APIVela getRequest = new APIVela();
        String api = "getUser";
        try {
            result = getRequest.execute(api).get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        if(result == null){
            Toast toast = Toast.makeText(this,"An error as occurred" , Toast.LENGTH_LONG);
            toast.show();
        } else {
            String firstName = "Not set";
            String lastName = "Not set";
            String phone = "Not set";
            String birthdate = "Not set";

            if(!result.equals(""))
            {
                JSONObject jsonObject;
                try {
                    jsonObject = new JSONObject(result);
                    firstName = jsonObject.getString("firstName");
                    lastName = jsonObject.getString("lastName");
                    phone = jsonObject.getString("phone");
                    birthdate = jsonObject.getString("birthDate");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Toast toast = Toast.makeText(this,"bipbop" , Toast.LENGTH_LONG);
                toast.show();
                infoSet = "PUT";
            }
            mEditFirstName.setText(firstName);
            mEditLastName.setText(lastName);
            mEditPhone.setText(phone);
            mEditBirthDate.setText(birthdate);
        }

        mEditFirstNameLayout = findViewById(R.id.profileFirstName);
        mEditLastNameLayout = findViewById(R.id.profileLastName);
        mEditPhoneLayout = findViewById(R.id.profilePhone);
        mEditBirthDateLayout = findViewById(R.id.profileBirthDate);

        mButton.setOnClickListener(this::saveInformations);
    }

    /**
     * @brief Save user information with API
     */
    public void saveInformations(View v){
        boolean isValid = true;
        if(GlobalContext.isTextValid(mEditFirstName.getText().toString()) == "empty"){
            mEditFirstNameLayout.setError("First name cannot be empty");
            isValid = false;
        } else if (GlobalContext.isTextValid(mEditFirstName.getText().toString()) == "nonAlpha"){
            mEditFirstNameLayout.setError("Some characters are not valid");
            isValid = false;
        }

        if(GlobalContext.isTextValid(mEditLastName.getText().toString()) == "empty"){
            mEditLastNameLayout.setError("Last name cannot be empty");
            isValid = false;
        } else if (GlobalContext.isTextValid(mEditLastName.getText().toString()) == "nonAlpha"){
            mEditLastNameLayout.setError("Some characters are not valid");
            isValid = false;
        }

        if(GlobalContext.isTextValid(mEditPhone.getText().toString()) == "empty"){
            mEditPhoneLayout.setError("Phone number cannot be empty");
            isValid = false;
        } else if (GlobalContext.isTextValid(mEditPhone.getText().toString()) == "nonAlpha"){
            mEditPhoneLayout.setError("Some characters are not valid");
            isValid = false;
        }

        if(GlobalContext.isTextValid(mEditBirthDate.getText().toString()) == "empty"){
            mEditBirthDateLayout.setError("Birthdate cannot be empty");
            isValid = false;
        } else if (GlobalContext.isTextValid(mEditBirthDate.getText().toString()) == "nonAlpha"){
            mEditBirthDateLayout.setError("Some characters are not valid");
            isValid = false;
        }

        if(!isValid)
            return;

        String firstName = mEditFirstName.getText().toString();
        String lastName = mEditLastName.getText().toString();
        String phone = mEditPhone.getText().toString();
        String birthdate = mEditBirthDate.getText().toString();
        String result = null;
        APIVela getRequest = new APIVela();
        String api = "editUser";
        try {
            result = getRequest.execute(api,firstName,lastName,phone,birthdate,infoSet).get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        super.finish();
    }
}