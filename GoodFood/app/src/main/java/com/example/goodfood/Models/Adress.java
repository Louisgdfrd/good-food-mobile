package com.example.goodfood.Models;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * @brief class to manipulate addresses
 */
public class Adress {

    public Adress(String id ,String number,String street,String postal,String city){
        this.id = id;
        this.number = number;
        this.street = street;
        this.postal = postal;
        this.city = city;

    }

    public Adress(JSONObject obj){
        try {
            this.id = obj.getString("id");
            this.number = obj.getString("number");
            this.street = obj.getString("street");
            this.postal = obj.getString("postal");
            this.city = obj.getString("city");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public Adress(){
        this.id = "0";
        this.number = "166";
        this.street = "cours berriat";
        this.postal = "38000";
        this.city = "Grenoble";
    }

    public String id;
    public String number;
    public String street;
    public String postal;
    public String city;

    public String toString(){return number + " " + street + ", " + city + " " + postal;}
}
