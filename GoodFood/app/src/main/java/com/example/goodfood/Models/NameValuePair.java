package com.example.goodfood.Models;

public class NameValuePair {
    public String mName;
    public String mValue;

    /**
     * @brief Constructor
     */
    public NameValuePair(String name, String value){
        mValue = value;
        mName = name;
    }

    /**
     * @brief get name parameter
     */
    public String getName() {
        return mName;
    }

    /**
     * @brief get value parameter
     */
    public String getValue() {
        return mValue;
    }
}
