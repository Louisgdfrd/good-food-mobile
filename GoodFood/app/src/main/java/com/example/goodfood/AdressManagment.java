package com.example.goodfood;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentContainerView;

public class AdressManagment extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    Button mButton;
    Button mOrderButton;
    Spinner mSpinner;
    FragmentContainerView mAdresses;

    /**
     * @brief set view elements
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adress_managment);
        mButton = findViewById(R.id.addAdressButton);
        mButton.setOnClickListener(this::AddAdress);
        if(GlobalContext.inOrder){
            mOrderButton = findViewById(R.id.orderAdressButton);
            mOrderButton.setOnClickListener(this::order);

            mSpinner = findViewById(R.id.typeOrderSpinner);
            ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                    R.array.order_type,
                    android.R.layout.simple_spinner_item);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mSpinner.setAdapter(adapter);
            mSpinner.setOnItemSelectedListener(this);
            mSpinner.setVisibility(View.VISIBLE);

            mAdresses = findViewById(R.id.containerAdressFragment);
        }
    }

    /**
     * @brief change the view to add adress
     */
    public void AddAdress(View v){
        Intent intent = new Intent(this, AddAdress.class);
        startActivity(intent);
        finish();
    }

    /**
     * @brief change the view for payment activity
     */
    public void order(View v){
        GlobalContext.orderAdress = GlobalContext.getRestaurantChoice().adress;
        Intent intent = new Intent(this, PaymentActivity.class);
        startActivity(intent);
    }

    /**
     * @brief set visibility of view elements
     */
    public void setVisibilityAdress(boolean visible){
        int visibilityAdress = View.GONE;
        int visibilityOrder = View.VISIBLE;
        if (visible) {
            visibilityAdress = View.VISIBLE;
            visibilityOrder = View.GONE;
        }
        mButton.setVisibility(visibilityAdress);
        mAdresses.setVisibility(visibilityAdress);
        mOrderButton.setVisibility(visibilityOrder);
    }

    /**
     * @brief set view elements
     */
    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        if (i == 0)
            setVisibilityAdress(true);
        else
            setVisibilityAdress(false);
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}