package com.example.goodfood;

import android.app.Application;

import com.example.goodfood.Models.Adress;
import com.example.goodfood.Models.Recipe;
import com.example.goodfood.Models.Restaurant;
import com.example.goodfood.Models.User;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class GlobalContext extends Application {

    private static Restaurant mRestaurantChoice = new Restaurant();
    private static ArrayList<Recipe> cart = new ArrayList<>();
    private static User mUser;
    private static Recipe mDetails;
    private static String token;
    public static Boolean inOrder;
    public static Adress orderAdress;

    public static Restaurant getRestaurantChoice(){return mRestaurantChoice;}

    /**
     * @brief set restaurant choose by the user
     */
    public static void setRestaurantChoice(Restaurant restaurantChoice) {
        clearCart();
        mRestaurantChoice = restaurantChoice;
    }

    /**
     * @brief set connection token
     */
    public static void setToken(String token) {GlobalContext.token = token;}

    /**
     * @brief return connection  token
     */
    public static String getToken() {return token;}

    /**
     * @brief return the cart
     */
    public static ArrayList<Recipe> getCart() {
        return cart;
    }

    /**
     * @brief return current user
     */
    public static User getUser() {
        return mUser;
    }

    /**
     * @brief set current User
     */
    public static void setUser(User user){
        mUser = user;
    }

    /**
     * @brief add a recipe to the cart
     */
    public static void addToCart(Recipe receipe){
        cart.add(receipe);
    }

    /**
     * @brief remove a recipe from the cart
     */
    public static void removeFromCart(int id){
        cart.remove(id);
    }

    /**
     * @brief clear the cart
     */
    public static void clearCart(){
        cart.clear();
    }

    public static DecimalFormat df = new DecimalFormat("0.00");

    /**
     * @brief return cart amount
     */
    public static String getCartAmount() {
        float total = 0;
        ArrayList<Recipe> cart = getCart();
        for(int i =0; i < cart.size();i++)
            total+= cart.get(i).getRealPrice();

        return df.format(total);
    }

    /**
     * @brief return if the text is valid
     */
    public static String isTextValid(String aString){
        if(aString.isEmpty())
            return "empty";
        else if (aString.matches("\\d{2}\\p{IsAlphabetic}{3}"))
            return "nonAlpha";
        else
            return "valid";
    }
}
