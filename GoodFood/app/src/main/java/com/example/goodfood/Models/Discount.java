package com.example.goodfood.Models;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
/**
 * @brief class to manipulate discount
 */
public class Discount {

    /**
     * Constructor for the class
     */
    public Discount(){
        this.value = 0;
    }

    public Discount(JSONObject json){
        try {
            id = json.getString("id");
            receipe = json.getString("receipe");
            receipeName = json.getString("receipeName");
            value = (float) json.getDouble("value");
            start = new Date();
            end = new Date();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public String id;
    public String receipe;
    public String receipeName;
    public float value;
    public Date start;
    public Date end;
}
