package com.example.goodfood;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.concurrent.ExecutionException;

public class FeedbackActivity extends AppCompatActivity {
    Button sendFeedBack;
    EditText comment;
    RatingBar ratingBar;

    /**
     * @brief set view elements
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
        ratingBar = findViewById(R.id.ratingFeedback);
        comment = findViewById(R.id.feedbackComment);
        sendFeedBack = findViewById(R.id.buttonSendFeedback);
        sendFeedBack.setOnClickListener(this::sendFeedback);
    }

    /**
     * @brief call sendFeedback API
     */
    public void sendFeedback(View v){
        String result = null;
        APIVela getRequest = new APIVela();
        String api = "sendFeedback";
        String feedback = comment.getText().toString();
        String rating =Float.toString(ratingBar.getRating());
        try {
            result = getRequest.execute(api, feedback,rating).get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
            Toast toast = Toast.makeText(this,"Thank's for your feedback !" , Toast.LENGTH_LONG);
            toast.show();
            Intent intent = new Intent(this, ChooseRestaurant.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK );
            startActivity(intent);
            finish();
    }
}