package com.example.goodfood;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

public class CartContainer extends Fragment {

    private static Button mButton;
    private static TextView mText;

    public CartContainer() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /**
     * @brief set view elements
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_cart_container, container, false);
        mButton = v.findViewById(R.id.cartOrderButton);
        mText = v.findViewById(R.id.emptyCartText);
        mButton.setOnClickListener(this::gotToPayment);
        setVisibility();
        return v;
    }

    /**
     * @brief set visibility of view elements
     */
    public static void setVisibility(){
        if(GlobalContext.getCart().size() == 0){
            mButton.setVisibility(View.INVISIBLE);
            mText.setVisibility(View.VISIBLE);
        } else {
            mButton.setVisibility(View.VISIBLE);
            mText.setVisibility(View.INVISIBLE);
        }
        mButton.setText("Pay " + GlobalContext.getCartAmount() + "€");
    }

    /**
     * @brief change view to complete order
     */
    public void gotToPayment(View v){
        GlobalContext.inOrder = true;
        Intent intent = new Intent(this.getContext(), AdressManagment.class);
        startActivity(intent);
    }
}