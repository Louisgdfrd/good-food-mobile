package com.example.goodfood.ui.address;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.goodfood.APIVela;
import com.example.goodfood.Models.Adress;
import com.example.goodfood.R;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

/**
 * A fragment representing a list of Items.
 */
public class AdressFragment extends Fragment {

    private static final String ARG_COLUMN_COUNT = "column-count";
    private int mColumnCount = 1;
    private ArrayList<Adress> adresses;
    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public AdressFragment() {
    }

    public static AdressFragment newInstance(int columnCount) {
        AdressFragment fragment = new AdressFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        populateAddresses();
        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }
    /**
     * @brief set view elements
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_adress_list, container, false);

        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
            if (mColumnCount <= 1) {
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }
            recyclerView.setAdapter(new MyAdressRecyclerViewAdapter(adresses));
        }
        return view;
    }

    private void populateAddresses(){
        adresses = new ArrayList<Adress>();
        String result = null;
        APIVela getRequest = new APIVela();
        String api = "getAdress";
        try {
            result = getRequest.execute(api).get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        if(result == null){
            Toast toast = Toast.makeText(this.getContext(),"An error as occurred" , Toast.LENGTH_LONG);
            toast.show();
        } else {
            try {
                JSONArray json = new JSONArray(result);
                for(int i = 0; i < json.length();i++)
                    adresses.add(new Adress(json.getJSONObject(i)));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}