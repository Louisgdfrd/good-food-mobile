package com.example.goodfood;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.google.android.material.textfield.TextInputLayout;

import java.util.concurrent.ExecutionException;

public class RegisterFragment extends Fragment {

    /**
     * Element of the view
     */
    EditText emailInput;
    EditText passwordInput;
    EditText repeatPasswordInput;
    TextInputLayout emailInputLayout;
    TextInputLayout passwordInputLayout;
    TextInputLayout repeatPasswordInputLayout;
    CheckBox mCheckBox;
    Button registerButton;
    UserLocalStore userLocalStore;

    public RegisterFragment() {
        // Required empty public constructor
    }

    /**
     * @brief
     * Create the view
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_register_activity, container, false);
        userLocalStore = new UserLocalStore(this.getContext());
        emailInput = view.findViewById(R.id.editTextSignUpEmail);
        passwordInput = view.findViewById(R.id.editTextSignUpPassword);
        repeatPasswordInput= view.findViewById(R.id.editTextSignUpConfirmPassword);
        mCheckBox= view.findViewById(R.id.consentRgpdRegister);

        emailInputLayout = view.findViewById(R.id.registerInputLayoutEmail);
        passwordInputLayout = view.findViewById(R.id.registerInputLayoutPassword);
        repeatPasswordInputLayout = view.findViewById(R.id.registerInputLayoutConfirm);

        registerButton = view.findViewById(R.id.buttonSignUpCreateAccount);
        registerButton.setOnClickListener(this::Register);
        return view;
    }

    /**
     * @brief
     * Call Register function from Back
     */
    public void Register(View v){
        boolean isValid = true;

        if(GlobalContext.isTextValid(emailInput.getText().toString()) == "empty"){
            emailInputLayout.setError("Email cannot be empty");
            isValid = false;
        } else
            emailInputLayout.setErrorEnabled(false);

        if(GlobalContext.isTextValid(passwordInput.getText().toString()) == "empty"){
            passwordInputLayout.setError("Password cannot be empty");
            isValid = false;
        }  else if (passwordInput.getText().toString().length() < 8 ){
            passwordInputLayout.setError("Password is too short");
            isValid = false;
        } else
            passwordInputLayout.setErrorEnabled(false);

        if(!repeatPasswordInput.getText().toString().equals(passwordInput.getText().toString())){
            repeatPasswordInputLayout.setError("The passwords are not the same");
            isValid = false;
        } else {
            repeatPasswordInputLayout.setErrorEnabled(false);
        }

        if (!mCheckBox.isChecked())
            isValid = false;

        if(!isValid)
            return;

        String result = null;
        APIVela getRequest = new APIVela();
        String api = "createAccount";
        String email = emailInput.getText().toString();
        String password = passwordInput.getText().toString();
        String consent = "true";
        try {
            result = getRequest.execute(api, email, password,consent
            ).get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        if(result == null){
            CharSequence text = "This email is already in use or there was a network issue";
            Toast toast = Toast.makeText(getActivity().getApplicationContext(),text , Toast.LENGTH_LONG);
            toast.show();
        } else {
            Toast toast = Toast.makeText(getActivity().getApplicationContext(),"Your account has been created" , Toast.LENGTH_LONG);
            toast.show();
        }
    }
}