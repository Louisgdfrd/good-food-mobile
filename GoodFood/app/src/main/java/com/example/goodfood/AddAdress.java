package com.example.goodfood;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputLayout;

import java.util.concurrent.ExecutionException;

public class AddAdress extends AppCompatActivity {

    private EditText mStreet;
    private EditText mStreetNumber;
    private EditText mCity;
    private EditText mPostal;
    private Button mButton;

    private TextInputLayout mStreetLayout;
    private TextInputLayout mStreetNumberLayout;
    private TextInputLayout mCityLayout;
    private TextInputLayout mPostalLayout;

    /**
     * @brief set view elements
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_adress);
        mButton = findViewById(R.id.addAdressButton);
        mStreet = findViewById(R.id.addAdressStreet);
        mStreetNumber = findViewById(R.id.addAdressStreetNumber);
        mCity = findViewById(R.id.addAdressCity);
        mPostal = findViewById(R.id.addAdressPostal);

        mStreetLayout = findViewById(R.id.addAdressStreetLayout);
        mStreetNumberLayout = findViewById(R.id.addAdressStreetNumberLayout);
        mCityLayout = findViewById(R.id.addAdressCityLayout);
        mPostalLayout = findViewById(R.id.addAdressPostalLayout);
        mButton.setOnClickListener(this::addAddress);
    }

    /**
     * @brief call addAdress API
     */
    public void addAddress(View v){
        boolean isValid = true;
        if(GlobalContext.isTextValid(mStreet.getText().toString()) == "empty"){
            mStreetLayout.setError("Name of the street cannot be empty");
            isValid = false;
        } else if (GlobalContext.isTextValid(mStreet.getText().toString()) == "nonAlpha"){
            mStreetLayout.setError("Some characters are not valid");
            isValid = false;
        }

        if(GlobalContext.isTextValid(mStreetNumber.getText().toString()) == "empty"){
            mStreetNumberLayout.setError("Street number cannot be empty");
            isValid = false;
        } else if (GlobalContext.isTextValid(mStreetNumber.getText().toString()) == "nonAlpha"){
            mStreetNumberLayout.setError("Some characters are not valid");
            isValid = false;
        }

        if(GlobalContext.isTextValid(mCity.getText().toString()) == "empty"){
            mCityLayout.setError("City cannot be empty");
            isValid = false;
        } else if (GlobalContext.isTextValid(mCity.getText().toString()) == "nonAlpha"){
            mCityLayout.setError("Some characters are not valid");
            isValid = false;
        }

        if(GlobalContext.isTextValid(mPostal.getText().toString()) == "empty"){
            mPostalLayout.setError("Postal cannot be empty");
            isValid = false;
        } else if (GlobalContext.isTextValid(mPostal.getText().toString()) == "nonAlpha"){
            mPostalLayout.setError("Some characters are not valid");
            isValid = false;
        }

        if(!isValid)
            return;

        String result = null;
        APIVela getRequest = new APIVela();
        String api = "addAdress";
        String number = mStreetNumber.getText().toString();
        String street = mStreet.getText().toString();
        String postal = mPostal.getText().toString();
        String city = mCity.getText().toString();
        try {
            result = getRequest.execute(api,number,street,postal,city).get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        if(result == null){
            Toast toast = Toast.makeText(this,"An error as occurred" , Toast.LENGTH_LONG);
            toast.show();
        } else {
            Intent intent = new Intent(this, AdressManagment.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED );
            startActivity(intent);
            super.finish();
        }
    }
}