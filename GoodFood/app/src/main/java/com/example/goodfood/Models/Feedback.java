package com.example.goodfood.Models;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * @brief class to manipulate Feedbacks
 */
public class Feedback {
    public Feedback(){

    }

    public String comment;
    public int eval;

    public Feedback(JSONObject jsonObject) {
        try {
            comment = jsonObject.getString("comment");
            eval = jsonObject.getInt("eval");
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
