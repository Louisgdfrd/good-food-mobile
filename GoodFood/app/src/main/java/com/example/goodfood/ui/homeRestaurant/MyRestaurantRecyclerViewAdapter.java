package com.example.goodfood.ui.homeRestaurant;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.goodfood.GlobalContext;
import com.example.goodfood.HomeActivity;
import com.example.goodfood.Models.Restaurant;
import com.example.goodfood.databinding.FragmentRestaurantDelegateBinding;

import java.util.List;

public class MyRestaurantRecyclerViewAdapter extends RecyclerView.Adapter<MyRestaurantRecyclerViewAdapter.ViewHolder> {

    private final List<Restaurant> mValues;
    private Context mContext;

    public MyRestaurantRecyclerViewAdapter(List<Restaurant> items, Context context) {
        mValues = items;
        mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        return new ViewHolder(FragmentRestaurantDelegateBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));

    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mRestaurantName.setText(mValues.get(position).name);
        holder.mRestaurantName.setTypeface(null, Typeface.BOLD);
        holder.mRestaurantAdress.setText(mValues.get(position).adress.toString());
        holder.mRestaurantPhone.setText(mValues.get(position).phone);
        if(mValues.get(position).discount.value == 0){
            holder.mCardDiscount.setVisibility(View.INVISIBLE);
            holder.mRestaurantDiscount.setVisibility(View.INVISIBLE);
        } else {
            holder.mRestaurantDiscount.setText(Float.toString(mValues.get(position).discount.value)+"%");
        }
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final TextView mRestaurantName;
        public final CardView mCard;
        public final CardView mCardDiscount;
        public final TextView mRestaurantAdress;
        public final TextView mRestaurantDiscount;
        public final TextView mRestaurantPhone;

        public int mPosition;
        public Restaurant mItem;

        public ViewHolder(FragmentRestaurantDelegateBinding binding) {
            super(binding.getRoot());
            mRestaurantName = binding.restaurantName;
            mRestaurantAdress = binding.restaurantAdress;
            mCard = binding.restaurantDelegateCard;
            mCardDiscount = binding.cardDiscount;
            mRestaurantDiscount = binding.RestaurantDiscount;
            mRestaurantPhone = binding.restaurantPhone;

            mCard.setOnClickListener(this::toasted);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mRestaurantAdress.getText() + "'";
        }

        public void toasted(View view){
            GlobalContext.setRestaurantChoice(mItem);
            Intent intent = new Intent(mContext, HomeActivity.class);
            mContext.startActivity(intent);
        }
    }
}