package com.example.goodfood.ui.receipe;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.goodfood.GlobalContext;
import com.example.goodfood.Models.Recipe;
import com.example.goodfood.R;

import java.util.ArrayList;

/**
 * A fragment representing a list of Items.
 */
public class ReceipeChoiceFragment extends Fragment {


    private static final String ARG_COLUMN_COUNT = "column-count";
    private int mColumnCount = 1;
    private ArrayList<Recipe> mReceipes = (ArrayList<Recipe>) GlobalContext.getRestaurantChoice().receipes;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ReceipeChoiceFragment() {
    }

    @SuppressWarnings("unused")
    public static ReceipeChoiceFragment newInstance(int columnCount) {
        ReceipeChoiceFragment fragment = new ReceipeChoiceFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_receipe_choice_list, container, false);
        /**
        String result = null;
        APIVela getRequest = new APIVela();
        String api = "getRestaurantReceipe";
        try {
            result = getRequest.execute(api).get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        if(result == null){

        } else {
            JSONArray restaurantRecipes = null;
            try {
                restaurantRecipes = new JSONArray(result);
                for(int i = 0; i < restaurantRecipes.length(); i++) {
                    Receipe newReceipe = new Receipe((JSONObject) restaurantRecipes.get(i));
                    mReceipes.add(newReceipe);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        */
        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
            if (mColumnCount <= 1) {
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }
            recyclerView.setAdapter(new MyReceipeRecyclerViewAdapter(mReceipes,this));
        }
        return view;
    }
}