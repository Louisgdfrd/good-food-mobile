package com.example.goodfood;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.goodfood.Models.Order;

import java.util.concurrent.ExecutionException;

public class PaymentActivity extends AppCompatActivity {

    private Button payButton;

    /**
     * @brief create the view
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        payButton = findViewById(R.id.cardPaymentButton);
        payButton.setOnClickListener(this::onPayClicked);
    }

    /**
     * @brief call order API
     */
    private void onPayClicked(View view) {

        String result = null;
        APIVela getRequest = new APIVela();
        String api = "order";
        Order order = new Order();
        try {
            result = getRequest.execute(api,order.sendOrderToJson()).get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }

        Toast toast = Toast.makeText(this, "Thank's for your order !", Toast.LENGTH_LONG);
        toast.show();
        Intent intent = new Intent(this, FeedbackActivity.class);
        startActivity(intent);
        finish();
    }
}