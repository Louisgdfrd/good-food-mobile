package com.example.goodfood.Models;
/**
 * @brief class to manipulate User
 */
public class User {
    public String id;
    public String email;
    public String password;
    public String firstName;
    public String lastName;
    public String phone;
    public String birthdate;

    /**
     * Constructor for the class
     */
    public User(String id,String email, String password, String firstName, String lastName, String phone , String birthdate){
        this.id = id;
        this.email = email;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phone = phone;
        this.birthdate = birthdate;
    }
}
