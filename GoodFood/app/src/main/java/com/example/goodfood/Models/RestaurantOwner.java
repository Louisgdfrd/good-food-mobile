package com.example.goodfood.Models;

import org.json.JSONException;
import org.json.JSONObject;
/**
 * @brief class to manipulate restaurant owner
 */
public class RestaurantOwner {
    /**
     * @brief Constructor
     */
    public RestaurantOwner(String mail,String firstName, String lastName){
        this.mail = mail;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public RestaurantOwner(JSONObject json){
        try {
            this.mail = json.getString("email");
            this.firstName = json.getString("firstName");
            this.lastName = json.getString("lastName");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String mail;
    public String firstName;
    public String lastName;
}
