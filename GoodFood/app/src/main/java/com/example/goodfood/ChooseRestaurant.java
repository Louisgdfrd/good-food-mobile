package com.example.goodfood;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

public class ChooseRestaurant extends AppCompatActivity {

    /**
     * @brief set view elements
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_restaurant);
    }
}