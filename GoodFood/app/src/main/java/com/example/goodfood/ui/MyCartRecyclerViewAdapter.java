package com.example.goodfood.ui;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.goodfood.CartContainer;
import com.example.goodfood.GlobalContext;
import com.example.goodfood.Models.Recipe;
import com.example.goodfood.databinding.FragmentCartItemBinding;

import java.util.List;

public class MyCartRecyclerViewAdapter extends RecyclerView.Adapter<MyCartRecyclerViewAdapter.ViewHolder> {

    private final List<Recipe> mValues;
    private final CartFragment mFragment;

    public MyCartRecyclerViewAdapter(List<Recipe> items, CartFragment mFragment) {
        mValues = items;
        this.mFragment = mFragment;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(FragmentCartItemBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, @SuppressLint("RecyclerView") int position) {
        holder.mItem = mValues.get(position);
        holder.mContentView.setText(mValues.get(position).name);
        holder.mPosition = position;
        if(mValues.get(position).photo != null) {
            byte[] imageAsBytes = Base64.decode(mValues.get(position).photo.getBytes(),Base64.DEFAULT);
            Bitmap bp = BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length);
            holder.mImageView.setImageBitmap(bp);
        }
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final TextView mContentView;
        public final ImageButton mButton;
        public final ImageView mImageView;
        public int mPosition;
        public Recipe mItem;

    public ViewHolder(FragmentCartItemBinding binding) {
      super(binding.getRoot());
      mContentView = binding.cartReceipeName;
      mButton = binding.buttonCart;
      mImageView = binding.imageCart;

      mButton.setOnClickListener(this::clicked);
    }

        public void clicked(View view){
            GlobalContext.removeFromCart(mPosition);
            notifyItemRemoved(mPosition);
            notifyItemRangeChanged(mPosition, GlobalContext.getCart().size());
            CartContainer.setVisibility();
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mContentView.getText() + "'";
        }
    }
}