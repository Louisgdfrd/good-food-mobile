package com.example.goodfood.ui.address;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.goodfood.APIVela;
import com.example.goodfood.GlobalContext;
import com.example.goodfood.Models.Adress;
import com.example.goodfood.PaymentActivity;
import com.example.goodfood.databinding.FragmentAdressDelegateBinding;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class MyAdressRecyclerViewAdapter extends RecyclerView.Adapter<MyAdressRecyclerViewAdapter.ViewHolder> {

    private final List<Adress> mValues;

    public MyAdressRecyclerViewAdapter(List<Adress> items) {
        mValues = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        return new ViewHolder(FragmentAdressDelegateBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));

    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, @SuppressLint("RecyclerView") int position) {
        holder.mItem = mValues.get(position);
        holder.mNumberText.setText(mValues.get(position).toString());
        holder.mPosition = position;
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final TextView mNumberText;
        public final ImageView mRemoveButton;
        public final CardView mCardViewAdress;
        public int mPosition;
        public Adress mItem;

        public ViewHolder(FragmentAdressDelegateBinding binding) {
            super(binding.getRoot());
            mNumberText = binding.adressNumber;
            mRemoveButton = binding.removeAdress;
            mCardViewAdress = binding.cardViewAdress;
            mRemoveButton.setOnClickListener(this::removeAdress);
            if(GlobalContext.inOrder)
                mCardViewAdress.setOnClickListener(this::selectAdress);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mNumberText.getText() + "'";
        }

        public void removeAdress(View v){
            String result = null;
            APIVela getRequest = new APIVela();
            String api = "removeAdress";
            String id = mItem.id;
            try {
                result = getRequest.execute(api, id).get();
            } catch (ExecutionException | InterruptedException e) {
                e.printStackTrace();
            }
            mValues.remove(mPosition);
            notifyItemRemoved(mPosition);
            notifyItemRangeChanged(mPosition, mValues.size());
        }

        public void selectAdress(View v){
            GlobalContext.orderAdress = mItem;
            Intent intent = new Intent(this.itemView.getContext(), PaymentActivity.class);
            this.itemView.getContext().startActivity(intent);
        }
    }
}