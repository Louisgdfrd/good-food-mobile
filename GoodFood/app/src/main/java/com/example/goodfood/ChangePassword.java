package com.example.goodfood;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.goodfood.Models.User;
import com.google.android.material.textfield.TextInputLayout;

import java.util.concurrent.ExecutionException;

public class ChangePassword extends AppCompatActivity {

    TextInputLayout oldPasswordLayout;
    TextInputLayout newPasswordLayout;
    TextInputLayout confirmPasswordLayout;
    EditText oldPassword;
    EditText newPassword;
    EditText confirmPassword;
    Button confirmButton;
    UserLocalStore userLocalStore;

    /**
     * @brief set view elements
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        userLocalStore = new UserLocalStore(this);
        oldPasswordLayout = findViewById(R.id.changePasswordOldLayout);
        newPasswordLayout = findViewById(R.id.changePasswordNewLayout);
        confirmPasswordLayout = findViewById(R.id.changePasswordConfirmLayout);
        oldPassword = findViewById(R.id.changePasswordOld);
        newPassword = findViewById(R.id.changePasswordNew);
        confirmPassword = findViewById(R.id.changePasswordConfirm);
        confirmButton = findViewById(R.id.changePasswordConfirmButton);
        confirmButton.setOnClickListener(this::ConfirmChange);
    }

    /**
     * @brief call Change password API
     */
    public void ConfirmChange(View v){
        boolean isValid = true;
        User user = userLocalStore.getLoggedInUser();

        if(!oldPassword.getText().toString().equals(user.password)){
            oldPasswordLayout.setError("Password do not match");
            isValid = false;
        } else
            oldPasswordLayout.setErrorEnabled(false);

        if(GlobalContext.isTextValid(newPassword.getText().toString()) == "empty"){
            newPasswordLayout.setError("Password cannot be empty");
            isValid = false;
        }  else if (newPassword.getText().toString().length() < 8 ){
            newPasswordLayout.setError("Password is too short");
            isValid = false;
        } else
            newPasswordLayout.setErrorEnabled(false);

        if(!confirmPassword.getText().toString().equals(newPassword.getText().toString())){
            confirmPasswordLayout.setError("The passwords are not the same");
            isValid = false;
        } else {
            confirmPasswordLayout.setErrorEnabled(false);
        }

        if(isValid){
            String result = null;
            APIVela getRequest = new APIVela();
            String api = "editPassword";
            try {
                result = getRequest.execute(api,oldPassword.getText().toString(),newPassword.getText().toString()).get();
            } catch (ExecutionException | InterruptedException e) {
                e.printStackTrace();
            }
            if(result == null){
                Toast toast = Toast.makeText(this,"An error as occurred" , Toast.LENGTH_LONG);
                toast.show();
            } else {
                User newUser = userLocalStore.getLoggedInUser();
                newUser.password = newPassword.getText().toString();
                userLocalStore.storeUserData(newUser);
                super.finish();
            }
        }
    }
}