var searchData=
[
  ['changeinformation_0',['changeInformation',['../classcom_1_1example_1_1goodfood_1_1ui_1_1_profile_1_1_profile_fragment.html#a2b93d56f8e468e81e9579534af19bbf2',1,'com::example::goodfood::ui::Profile::ProfileFragment']]],
  ['changepassword_1',['changePassword',['../classcom_1_1example_1_1goodfood_1_1ui_1_1_profile_1_1_profile_fragment.html#abeef4b13b6dcbf8ff4ac09e6efbb777a',1,'com::example::goodfood::ui::Profile::ProfileFragment']]],
  ['changevisibility_2',['changeVisibility',['../classcom_1_1example_1_1goodfood_1_1ui_1_1receipe_1_1_my_receipe_recycler_view_adapter_1_1_view_holder.html#a867004975b9d9a9a2f0a97d4ac70be89',1,'com::example::goodfood::ui::receipe::MyReceipeRecyclerViewAdapter::ViewHolder']]],
  ['clearcart_3',['clearCart',['../classcom_1_1example_1_1goodfood_1_1_global_context.html#a4540504732562328f6b1c4866ca74795',1,'com::example::goodfood::GlobalContext']]],
  ['clearuserdata_4',['clearUserData',['../classcom_1_1example_1_1goodfood_1_1_user_local_store.html#af3fd2a3be8c187c06a76716779412bf7',1,'com::example::goodfood::UserLocalStore']]],
  ['confirmchange_5',['ConfirmChange',['../classcom_1_1example_1_1goodfood_1_1_change_password.html#a1a0abf693cecff36349891c8bb970db9',1,'com::example::goodfood::ChangePassword']]],
  ['createaccount_6',['createAccount',['../classcom_1_1example_1_1goodfood_1_1_a_p_i_vela.html#a7f4c3c8bb92c51114b4523aef1be2b27',1,'com::example::goodfood::APIVela']]]
];
