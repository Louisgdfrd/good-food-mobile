var searchData=
[
  ['saveinformations_0',['saveInformations',['../classcom_1_1example_1_1goodfood_1_1_modify_profile.html#a29719a255534d0864c3a6a884afd6cb5',1,'com::example::goodfood::ModifyProfile']]],
  ['sendfeedback_1',['sendFeedback',['../classcom_1_1example_1_1goodfood_1_1_a_p_i_vela.html#a9ddb01350fc260a9e3e94b0503f23d17',1,'com.example.goodfood.APIVela.sendFeedback()'],['../classcom_1_1example_1_1goodfood_1_1_feedback_activity.html#aa0764a2e9424f43c56f13a1c50d47cdc',1,'com.example.goodfood.FeedbackActivity.sendFeedback()']]],
  ['setrestaurantchoice_2',['setRestaurantChoice',['../classcom_1_1example_1_1goodfood_1_1_global_context.html#a34063b9821113bb4e7555336cc63c39e',1,'com::example::goodfood::GlobalContext']]],
  ['settoken_3',['setToken',['../classcom_1_1example_1_1goodfood_1_1_global_context.html#a09c6d2afaf388c6fc76aa9fc9507ed6e',1,'com::example::goodfood::GlobalContext']]],
  ['setuser_4',['setUser',['../classcom_1_1example_1_1goodfood_1_1_global_context.html#a50abef5f1b77f39cbe27fa5be034d435',1,'com::example::goodfood::GlobalContext']]],
  ['setuserloggedin_5',['setUserLoggedIn',['../classcom_1_1example_1_1goodfood_1_1_user_local_store.html#a009c2bd31f6b5eb6c6bf5770f08f1eef',1,'com::example::goodfood::UserLocalStore']]],
  ['setvisibility_6',['setVisibility',['../classcom_1_1example_1_1goodfood_1_1_cart_container.html#aa26eac841764360a207b511ce84fc2c1',1,'com::example::goodfood::CartContainer']]],
  ['setvisibilityadress_7',['setVisibilityAdress',['../classcom_1_1example_1_1goodfood_1_1_adress_managment.html#ac954fa63150f9b79a0d165ba5c086e58',1,'com::example::goodfood::AdressManagment']]],
  ['storeuserdata_8',['storeUserData',['../classcom_1_1example_1_1goodfood_1_1_user_local_store.html#a703b724df24b616ba83e34ba27b56fe5',1,'com::example::goodfood::UserLocalStore']]],
  ['supplier_9',['Supplier',['../classcom_1_1example_1_1goodfood_1_1_models_1_1_supplier.html#a3783feab6ebecadc691bacbd0b9d4cf1',1,'com::example::goodfood::Models::Supplier']]]
];
