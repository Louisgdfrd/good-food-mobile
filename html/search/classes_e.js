var searchData=
[
  ['receipechoicefragment_0',['ReceipeChoiceFragment',['../classcom_1_1example_1_1goodfood_1_1ui_1_1receipe_1_1_receipe_choice_fragment.html',1,'com::example::goodfood::ui::receipe']]],
  ['recipe_1',['Recipe',['../classcom_1_1example_1_1goodfood_1_1_models_1_1_recipe.html',1,'com::example::goodfood::Models']]],
  ['registerfragment_2',['RegisterFragment',['../classcom_1_1example_1_1goodfood_1_1_register_fragment.html',1,'com::example::goodfood']]],
  ['restaurant_3',['Restaurant',['../classcom_1_1example_1_1goodfood_1_1_models_1_1_restaurant.html',1,'com::example::goodfood::Models']]],
  ['restaurantfragment_4',['RestaurantFragment',['../classcom_1_1example_1_1goodfood_1_1ui_1_1home_restaurant_1_1_restaurant_fragment.html',1,'com::example::goodfood::ui::homeRestaurant']]],
  ['restaurantowner_5',['RestaurantOwner',['../classcom_1_1example_1_1goodfood_1_1_models_1_1_restaurant_owner.html',1,'com::example::goodfood::Models']]]
];
