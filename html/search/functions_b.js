var searchData=
[
  ['receipechoicefragment_0',['ReceipeChoiceFragment',['../classcom_1_1example_1_1goodfood_1_1ui_1_1receipe_1_1_receipe_choice_fragment.html#acf3983604c820f7e7a43b17907cf8e20',1,'com::example::goodfood::ui::receipe::ReceipeChoiceFragment']]],
  ['recipe_1',['Recipe',['../classcom_1_1example_1_1goodfood_1_1_models_1_1_recipe.html#a29e49c56084deb9eb80d1c4db4e64a46',1,'com::example::goodfood::Models::Recipe']]],
  ['register_2',['Register',['../classcom_1_1example_1_1goodfood_1_1_register_fragment.html#ab3fb5b2aba5d4a6863d3ce0d2ea94443',1,'com::example::goodfood::RegisterFragment']]],
  ['removeadress_3',['removeAdress',['../classcom_1_1example_1_1goodfood_1_1_a_p_i_vela.html#a39e6784ba6a529949efe8e4076f296f7',1,'com::example::goodfood::APIVela']]],
  ['removefromcart_4',['removeFromCart',['../classcom_1_1example_1_1goodfood_1_1_global_context.html#ac43b92eac1840ff2d9bf4ae80efc4798',1,'com::example::goodfood::GlobalContext']]],
  ['restaurant_5',['Restaurant',['../classcom_1_1example_1_1goodfood_1_1_models_1_1_restaurant.html#a14e523bc277f20b99f0c967730c59009',1,'com::example::goodfood::Models::Restaurant']]],
  ['restaurantowner_6',['RestaurantOwner',['../classcom_1_1example_1_1goodfood_1_1_models_1_1_restaurant_owner.html#ade7d84a02b7b55647d133d1a723a7955',1,'com::example::goodfood::Models::RestaurantOwner']]]
];
