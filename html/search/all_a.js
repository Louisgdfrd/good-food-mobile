var searchData=
[
  ['mainactivity_0',['MainActivity',['../classcom_1_1example_1_1goodfood_1_1_main_activity.html',1,'com::example::goodfood']]],
  ['manageadresses_1',['manageAdresses',['../classcom_1_1example_1_1goodfood_1_1ui_1_1_profile_1_1_profile_fragment.html#a42ba66b0f8386db5c4b0744055c3c47c',1,'com::example::goodfood::ui::Profile::ProfileFragment']]],
  ['modifyprofile_2',['ModifyProfile',['../classcom_1_1example_1_1goodfood_1_1_modify_profile.html',1,'com::example::goodfood']]],
  ['myadressrecyclerviewadapter_3',['MyAdressRecyclerViewAdapter',['../classcom_1_1example_1_1goodfood_1_1ui_1_1address_1_1_my_adress_recycler_view_adapter.html',1,'com::example::goodfood::ui::address']]],
  ['mycartrecyclerviewadapter_4',['MyCartRecyclerViewAdapter',['../classcom_1_1example_1_1goodfood_1_1ui_1_1_my_cart_recycler_view_adapter.html',1,'com::example::goodfood::ui']]],
  ['myreceiperecyclerviewadapter_5',['MyReceipeRecyclerViewAdapter',['../classcom_1_1example_1_1goodfood_1_1ui_1_1receipe_1_1_my_receipe_recycler_view_adapter.html',1,'com::example::goodfood::ui::receipe']]],
  ['myrestaurantrecyclerviewadapter_6',['MyRestaurantRecyclerViewAdapter',['../classcom_1_1example_1_1goodfood_1_1ui_1_1home_restaurant_1_1_my_restaurant_recycler_view_adapter.html',1,'com::example::goodfood::ui::homeRestaurant']]]
];
