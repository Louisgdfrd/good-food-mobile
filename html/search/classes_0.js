var searchData=
[
  ['activityaddadressbinding_0',['ActivityAddAdressBinding',['../classcom_1_1example_1_1goodfood_1_1databinding_1_1_activity_add_adress_binding.html',1,'com::example::goodfood::databinding']]],
  ['activityadressmanagmentbinding_1',['ActivityAdressManagmentBinding',['../classcom_1_1example_1_1goodfood_1_1databinding_1_1_activity_adress_managment_binding.html',1,'com::example::goodfood::databinding']]],
  ['activitychangepasswordbinding_2',['ActivityChangePasswordBinding',['../classcom_1_1example_1_1goodfood_1_1databinding_1_1_activity_change_password_binding.html',1,'com::example::goodfood::databinding']]],
  ['activitychooserestaurantbinding_3',['ActivityChooseRestaurantBinding',['../classcom_1_1example_1_1goodfood_1_1databinding_1_1_activity_choose_restaurant_binding.html',1,'com::example::goodfood::databinding']]],
  ['activityfeedbackbinding_4',['ActivityFeedbackBinding',['../classcom_1_1example_1_1goodfood_1_1databinding_1_1_activity_feedback_binding.html',1,'com::example::goodfood::databinding']]],
  ['activityhomebinding_5',['ActivityHomeBinding',['../classcom_1_1example_1_1goodfood_1_1databinding_1_1_activity_home_binding.html',1,'com::example::goodfood::databinding']]],
  ['activitymainbinding_6',['ActivityMainBinding',['../classcom_1_1example_1_1goodfood_1_1databinding_1_1_activity_main_binding.html',1,'com::example::goodfood::databinding']]],
  ['activitymodifyprofilebinding_7',['ActivityModifyProfileBinding',['../classcom_1_1example_1_1goodfood_1_1databinding_1_1_activity_modify_profile_binding.html',1,'com::example::goodfood::databinding']]],
  ['activitypaymentbinding_8',['ActivityPaymentBinding',['../classcom_1_1example_1_1goodfood_1_1databinding_1_1_activity_payment_binding.html',1,'com::example::goodfood::databinding']]],
  ['addadress_9',['AddAdress',['../classcom_1_1example_1_1goodfood_1_1_add_adress.html',1,'com::example::goodfood']]],
  ['adress_10',['Adress',['../classcom_1_1example_1_1goodfood_1_1_models_1_1_adress.html',1,'com::example::goodfood::Models']]],
  ['adressfragment_11',['AdressFragment',['../classcom_1_1example_1_1goodfood_1_1ui_1_1address_1_1_adress_fragment.html',1,'com::example::goodfood::ui::address']]],
  ['adressmanagment_12',['AdressManagment',['../classcom_1_1example_1_1goodfood_1_1_adress_managment.html',1,'com::example::goodfood']]],
  ['apivela_13',['APIVela',['../classcom_1_1example_1_1goodfood_1_1_a_p_i_vela.html',1,'com::example::goodfood']]],
  ['apivelatest_14',['APIVelaTest',['../classcom_1_1example_1_1goodfood_1_1_a_p_i_vela_test.html',1,'com::example::goodfood']]]
];
